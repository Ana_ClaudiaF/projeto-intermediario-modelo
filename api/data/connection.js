const mongoose = require('mongoose');
const urlDB = "mongodb://localhost/intermediario";
const options = {
                    useUnifiedTopology: true,
                    useNewUrlParser: true
                };

mongoose.connect(urlDB, options);
mongoose.Promise = global.Promise;

module.exports = mongoose;