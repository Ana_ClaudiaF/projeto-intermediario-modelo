const mongoose = require("../data/connection");

const charactersSchema = new mongoose.Schema({
    name:{
        type: String,
        required: true
    },
    cast:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Cast',
        default: null
    },
    film:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Film',
        default: null
    },
    createAt:{
        type: Date,
        default: Date.now
    }
});

const characters = mongoose.model('Characters', charactersSchema);

module.exports = characters;