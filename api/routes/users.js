const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');

const config = require("../data/auth");

const User = require('../models/users');

function generateToken(params = {}){console.log(config.secret)
    return jwt.sign(params, config.secret);
}

router.post('/save', async (req, res) => {
    try {
        const user = await User.create(req.body);

        return res.send({user, token: generateToken({id: user.id})});
    }

    catch (err) {
        return res.status(400).send({error: "Problemas ao registrar o usuário"});
    }
});

router.post("/authenticate", async (req, res) =>{
    const {name, password} = req.body;
console.log(req.body)
    const user = await User.findOne({name}).select('+password');

    if (!user){
        return res.status(400).send({error:"Usuário não encontrado"});
    }

    if (name != user.name){
        return res.status(400).send({error:"Usuário inválido"});
    }

    if (password != user.password) {
        return res.status(400).send({error:"Senha inválida"});
    }
console.log(user)
    res.send({user, token: generateToken(user.id)});
});

module.exports = router;