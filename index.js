const PORT = 3000;

const routes =  {
    auth: require("./api/routes/authenticate"),
    users: require("./api/routes/users"),
    film: require("./api/routes/film"),
    studio: require("./api/routes/studio"),
    cast: require("./api/routes/cast"),
    producers: require("./api/routes/producers"),
    characters: require("./api/routes/characters"),
    editors: require("./api/routes/editors")
};

const express = require('express');
const app = express();
const bodyParser = require('body-parser');

app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use("/api/auth", routes.auth);
app.use("/api/usuarios", routes.users);
app.use("/api/filmes", routes.film);
app.use("/api/estudios", routes.studio);
app.use("/api/elenco", routes.cast);
app.use("/api/produtores", routes.producers);
app.use("/api/personagens", routes.characters);
app.use("/api/editores", routes.editors);

app.listen(PORT, () => console.log("Executando ...."));